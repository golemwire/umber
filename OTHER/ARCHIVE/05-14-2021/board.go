package main

type Board [8][8]Piece

func (b *Board)String() string {
	// This is generated left-to-right, top-to-bottom
	
	// The first line plus first number
	ret := "  A B C D E F G H\n8 "
	for y := 0; y < 8; y++ {
		// Add chess pieces:
		for x := 0; x < 8; x++ {
			ret += b[y][x].String()
		}
		// Add number at the right side of board, newline, then number at the left side of the board at the beginning of the new line...
		ret += [...]string{
			" 8\n7 ",
			" 7\n6 ",
			" 6\n5 ",
			" 5\n4 ",
			" 4\n3 ",
			" 3\n2 ",
			" 2\n1 ",
			" 1\n  A B C D E F G H", // ...however, the last one is different.
		}[y]
	}
	return ret
}
