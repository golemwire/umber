package main

/*
	Contains these functions relating to rule legality:
	•	moves: runs a callback on the places a square (which should be occupied) can move.
	•	isLegal: takes a start and a destination, and tells whether that move is legal.
	•	canBeMovedTo: tells whether a square (occupied or not) can be moved to. (TODO)
*/

// Represents the extra action of some chess moves:
// Castling: moves the rook over the king. (TODO, for .moves and .isLegal)
// En passant: deletes the captured pawn. (TODO)
// Pawn promotion: replaces the promoted pawn. (TODO)
// 
// See https://en.wikipedia.org/wiki/Chess
type specialMove_t struct {
	specialMoveType specialMoveType_t
	deletePiece coord
	setPiece_piece Piece
	setPiece_coord coord
}
type specialMoveType_t uint8
const (
	specialMove_castle specialMoveType_t = iota
	specialMove_enPassant
	specialMove_promotion
)

// Calls legalMove for each legal move, not considering check. Use checkCheck() for that.
// The point of having these checks separate is because checkCheck() is slow.
// 
// s.color must be the color of the piece at s.c.
func (s *Scene)moves(legalMove func(c coord, specialMove *specialMove_t)(stopEnumerating bool)) {
	piece := s.board[s.c.y][s.c.x]
	
	slide := func(directions []coord) /*stop enumerating*/ bool {
		var c coord
		
		for dirI := range directions {
			c = s.c
			for {
				c = c.plus(directions[dirI])
				
				if !c.withinBounds() { break }
				// Cannot capture your own piece:
				if s.board[c.y][c.x].pType != piece_nil && s.board[c.y][c.x].color == s.color { break }
				
				if legalMove(c, nil) {
					return true
				}
				
				// If a piece was captured, stop sliding:
				if s.board[c.y][c.x].pType != piece_nil && s.board[c.y][c.x].color != s.color { break }
			}
		}
		return false
	}
	pieceDirections := [...]coord{
		// Straight, rook-like:
		{-1,  0}, { 1,  0}, { 0, -1}, { 0,  1},
		// Diagonal, bishop-like:
		{-1, -1}, {-1,  1}, { 1,  1}, { 1, -1},
	}
	
	if insanityChecks {
		switch {
		case piece.pType == piece_nil: panic("Piece being moved is piece_nil")
		}
	}
	
	switch {
	// Can't move a non-existant piece:
	case piece.pType == piece_nil: return
	// Can't move opponent's piece:
	case s.color != piece.color: return
	}
	
	
	
	switch piece.pType {
	case pawn:
		switch piece.color {
		case black:
			if s.c.y == 7 { return }
			
			// Diagonal capture:
			if s.c.x != 0 {
				if s.board[s.c.y + 1][s.c.x - 1].pType != piece_nil && s.board[s.c.y + 1][s.c.x - 1].color != s.color {
					if legalMove(coord{s.c.x - 1, s.c.y + 1}, nil) { return }
				}
			}
			if s.c.x != 7 {
				if s.board[s.c.y + 1][s.c.x + 1].pType != piece_nil && s.board[s.c.y + 1][s.c.x + 1].color != s.color {
					if legalMove(coord{s.c.x + 1, s.c.y + 1}, nil) { return }
				}
			}
			
			// One square ahead:
			if s.board[s.c.y + 1][s.c.x].pType == piece_nil {
				if legalMove(coord{s.c.x, s.c.y + 1}, nil) { return }
				
				// Two squares ahead, if they're empty, and the pawn is on its first row:
				if s.c.y == 1 && s.board[3][s.c.x].pType == piece_nil {
					legalMove(coord{s.c.x, 3}, nil)
				}
			}
			
		case white:
			if s.c.y == 0 { return }
			
			// Diagonal capture:
			if s.c.x != 0 {
				if s.board[s.c.y - 1][s.c.x - 1].pType != piece_nil && s.board[s.c.y - 1][s.c.x - 1].color != s.color {
					if legalMove(coord{s.c.x - 1, s.c.y - 1}, nil) { return }
				}
			}
			if s.c.x != 7 {
				if s.board[s.c.y - 1][s.c.x + 1].pType != piece_nil && s.board[s.c.y - 1][s.c.x + 1].color != s.color {
					if legalMove(coord{s.c.x + 1, s.c.y - 1}, nil) { return }
				}
			}
			
			// One square ahead:
			if s.board[s.c.y - 1][s.c.x].pType == piece_nil {
				if legalMove(coord{s.c.x, s.c.y - 1}, nil) { return }
				
				// Two squares ahead, if they're empty, and the pawn is on its first row:
				if s.c.y == 6 && s.board[4][s.c.x].pType == piece_nil {
					legalMove(coord{s.c.x, 4}, nil)
				}
			}
		}
	case knight:
		for _, c := range [...]coord{{-2, -1}, {-1, -2}, {1, -2}, {2, -1}, {2, 1}, {1, 2}, {-1, 2}, {-2, 1}} {
			c = s.c.plus(c)
			
			if !c.withinBounds() { continue }
			// Cannot capture your own piece:
			if s.board[c.y][c.x].pType != piece_nil && s.board[c.y][c.x].color == s.color { continue }
			
			if legalMove(c, nil) { return }
		}
	case bishop:
		slide(pieceDirections[4:])
	case rook:
		slide(pieceDirections[:4])
	case queen:
		slide(pieceDirections[:])
	case king:
		var x, y int8
		for y = -1; y <= 1; y++ {
			for x = -1; x <= 1; x++ {
				c := s.c.plus(coord{x, y})
				
				if !c.withinBounds() { continue }
				// Cannot capture your own piece:
				if s.board[c.y][c.x].pType != piece_nil && s.board[c.y][c.x].color == s.color { continue }
				
				if legalMove(c, nil) { return }
			}
		}
		
		switch s.color {
		case white:
			if !s.getField(scene_whiteKingMoved) {
				specialMove := specialMove_t{specialMoveType: specialMove_castle}
				
				// Right castle:
				if !s.getField(scene_rook7_7) && s.board[7][6].pType == piece_nil && s.board[7][5].pType == piece_nil {
					specialMove.deletePiece = coord{7, 7}
					specialMove.setPiece_piece = Piece{white, rook}
					specialMove.setPiece_coord = coord{5, 7}
					if legalMove(coord{6, 7}, &specialMove) { return }
				}
				
				// Left castle:
				if !s.getField(scene_rook0_7) && s.board[7][1].pType == piece_nil && s.board[7][2].pType == piece_nil && s.board[7][3].pType == piece_nil {
					specialMove.deletePiece = coord{0, 7}
					specialMove.setPiece_piece = Piece{white, rook}
					specialMove.setPiece_coord = coord{3, 7}
					if legalMove(coord{6, 7}, &specialMove) { return }
				}
			}
		case black:
			if !s.getField(scene_blackKingMoved) {
				specialMove := specialMove_t{specialMoveType: specialMove_castle}
				
				// Right castle:
				if !s.getField(scene_rook7_0) && s.board[0][6].pType == piece_nil && s.board[0][5].pType == piece_nil {
					specialMove.deletePiece = coord{7, 0}
					specialMove.setPiece_piece = Piece{white, rook}
					specialMove.setPiece_coord = coord{5, 0}
					if legalMove(coord{6, 0}, &specialMove) { return }
				}
				
				// Left castle:
				if !s.getField(scene_rook0_0) && s.board[0][1].pType == piece_nil && s.board[0][2].pType == piece_nil && s.board[0][3].pType == piece_nil {
					specialMove.deletePiece = coord{0, 0}
					specialMove.setPiece_piece = Piece{black, rook}
					specialMove.setPiece_coord = coord{3, 0}
					if legalMove(coord{6, 0}, &specialMove) { return }
				}
			}
		}
	}
}

// Just returns whether a move from c.x,c.y to move (param) is legal, not considering check. Use checkCheck() for that.
func (s *Scene)isLegal(move coord) bool {
	piece := s.board[s.c.y][s.c.x]
	
	// For the "sliding"? pieces: the rook, bishop, and queen.
	var dist, x, y, xVel, yVel int8
	x, y = s.c.x, s.c.y
	slide := func() bool {
		// Stop one square short, because it may be occupied (like if capturing)
		dist--
		for i := 0; i < int(dist); i++ {
			// Also, skip the first square (because it is occupied by the moving piece)
			x += xVel ; y += yVel
			
			if s.board[y][x].pType != piece_nil {
				// Cannot move through a piece
				return false
			}
		}
		return true
	}
	// For the rook & queen.
	// (Returns whether the move is definitely illegal.)
	straightSlideInit := func() bool {
		if s.c.x == move.x {
			if move.y < s.c.y {
				yVel = -1
				dist = s.c.y - move.y
			} else {
				yVel = 1
				dist = move.y - s.c.y
			}
			return true
		} else if s.c.y == move.y {
			if move.x < s.c.x {
				xVel = -1
				dist = s.c.x - move.x
			} else {
				xVel = 1
				dist = move.x - s.c.x
			}
			return true
		}
		return false
	}
	// For the bishop & queen.
	diagonalSlideInit := func() bool {
		if move.x - s.c.x == move.y - s.c.y { // backslash-diagonal
			if move.x < s.c.x {
				xVel = -1
				yVel = -1
				dist = s.c.x - move.x
			} else {
				xVel = 1
				yVel = 1
				dist = move.x - s.c.x
			}
			return true
		} else if move.x - s.c.x == s.c.y - move.y { // forwardslash-diagonal
			if move.x < s.c.x {
				xVel = -1
				yVel = 1
				dist = s.c.x - move.x
			} else {
				xVel = 1
				yVel = -1
				dist = move.x - s.c.x
			}
			return true
		}
		return false
	}
	
	switch {
	// Can't move a non-existant piece:
	case piece.pType == piece_nil:
		return false
	/*// Can't move a piece to itself: REDUNDANT
	case s.x==moveX && s.y==moveY:
		return false*/
	// Can't move opponent's piece:
	case s.color != piece.color:
		return false
	// Can't capture your own piece:
	case s.board[move.y][move.x].pType!=piece_nil && s.board[move.y][move.x].color == s.color:
		return false
	}
	
	switch piece.pType {
	case pawn:
		// Moving one square ahead (white) or backwards (black):
		if (s.color == white && s.c.y-1 == move.y) ||
		   (s.color == black && s.c.y+1 == move.y) {
			switch {
			// Moving forward one (or two) moves w/o capture:
			case s.c.x == move.x:
				return s.board[move.y][move.x].pType == piece_nil
			// Capturing diagonally:
			case s.c.x-1==move.x || s.c.x+1==move.x:
				return s.board[move.y][move.x].pType != piece_nil &&
				       s.board[move.y][move.x].color != s.color
			default:
				return false
			}
		} else if (s.color == white && s.c.y == 6 && move.y == 4 && s.board[5][move.x].pType == piece_nil) ||
		          (s.color == black && s.c.y == 1 && move.y == 3 && s.board[2][move.x].pType == piece_nil) { // Moving two squares ahead, and not jumping over a piece:
			// Not moving to the left or the right, and not capturing, and not jumping over a piece:
			return s.c.x == move.x && s.board[move.y][move.x].pType == piece_nil
		}
	case knight:
		if (move.x == s.c.x-2 && move.y == s.c.y-1) ||
		   (move.x == s.c.x-2 && move.y == s.c.y+1) ||
		   (move.x == s.c.x+2 && move.y == s.c.y-1) ||
		   (move.x == s.c.x+2 && move.y == s.c.y+1) ||
		   (move.x == s.c.x-1 && move.y == s.c.y-2) ||
		   (move.x == s.c.x+1 && move.y == s.c.y+2) ||
		   (move.x == s.c.x-1 && move.y == s.c.y+2) ||
		   (move.x == s.c.x+1 && move.y == s.c.y-2) {
			return true
		}
	case bishop:
		if diagonalSlideInit() == false {return false}
		
		return slide()
	case rook:
		if straightSlideInit() == false {return false}
		
		return slide()
	case queen:
		/*ssi := straightSlideInit()
		var sSlide bool
		if ssi {sSlide = slide()}
		dsi := diagonalSlideInit()
		var dSlide bool
		if dsi {dSlide = slide()}
		return (ssi && sSlide) || (dsi && dSlide)*/
		ss := straightSlideInit() && slide()
		ds := diagonalSlideInit() && slide()
		
		return ss || ds
	case king:
		// Moving within 1 square (including diagonally)
		if move.x >= s.c.x-1 && move.x <= s.c.x+1 &&
		   move.y >= s.c.y-1 && move.y <= s.c.y+1 {
			return true
		}
	
	default: panic("FATAL")
	}
	
	//fmt.Fprintf(os.Stderr, "Error: exited main isLegal switch\n")
	return false
}

// Returns whether s.color is in check. Rather slow, call infrequently.
// Modifies the scene while working, but reverts changes.
func (s *Scene)checkCheck() bool {
	// Find the king:
	var theKing coord
	var i coord
	for i.y = 0; i.y < 8; i.y++ {
		for i.x = 0; i.x < 8; i.x++ {
			if s.board[i.y][i.x].pType == king && s.board[i.y][i.x].color == s.color {
				theKing = i
				goto kingFound
			}
			/*if s.board[i.y][i.x].pType == king {
				if s.board[i.y][i.x].color == black {
					blackKing = i
				} else {
					whiteKing = i
				}
			}*/
		}
	}
	return false // There is no king of this color
	kingFound:
	
	s_cCopy := s.c
	s_colorCpy := s.color
	s.color = 1-s.color
	for i.y = 0; i.y < 8; i.y++ {
		for i.x = 0; i.x < 8; i.x++ {
			s.c = i
			
			if s.board[i.y][i.x].pType == piece_nil { continue }
			
			if s.board[i.y][i.x].color == s.color {
				if s.isLegal(theKing) {
					return true
				}
			}
			
			/*case black:
				s.color = black
				if s.isLegal(whiteKing) {
				//	s.setField(scene_whiteCheck)
					whiteCheck = true
				}
			case white:
				s.color = white
				if s.isLegal(blackKing) {
				//	s.setField(scene_blackCheck)
					blackCheck = true
				}
			}*/
		}
	}
	s.c = s_cCopy
	s.color = s_colorCpy
	
	return false
}

// LATER:
/*// Returns whether the square can be moved to.
func (s *Scene)canBeMovedTo() bool {
	// Scan outward, scanning the row and column for pieces which can potentially move to here.
	// Also check for knights that could move here.
	// 
	
	// and run isLegal on it.
}*/
