package main

import (
	"math"
)

type Consideration struct {
	score float64
	start, end coord
	specialMoveType specialMoveType_t
}

// Attempts to find the best move that the given color can play on the given board.
// Returns that move, and how many possible moves there were. If it is 0, then the returned move is undefined.
func (s Scene)goodMoves(depth, startDepth int) (bestIdea Consideration, moves, calculations int) {
	considerationCount := 0
	var considerations chan struct{c []Consideration; calculations int}
	var considerationsArray [64/*Amount of chess board squares*/][]Consideration
	
	// If this is the first or second level of depth, then use goroutines and chans:
	concurrency := (depth==startDepth)||(depth-1 == startDepth)
	// This means that a chan should be used rather than an array:
	if concurrency {
		considerations = make(chan struct{c []Consideration; calculations int})
	}
	//ideas = make([]Consideration, 16)[:0]
	
	var c coord
	for c.y = 0; c.y < 8; c.y++ {
		for c.x = 0; c.x < 8; c.x++ {
			if s.board[c.y][c.x].pType == piece_nil {continue}
			
			if s.board[c.y][c.x].color == s.color {
				if concurrency {
					go func(s Scene) {
						ideas, calculations2 := s.consider(depth, startDepth)
						considerations <- struct{c []Consideration; calculations int}{ideas, calculations2}
					}(Scene{
						board: s.board,
						c: c,
						color: s.color,
						fields: s.fields,
					})
				} else {
					// same, but chan-less and not in a goroutine:
					var calculations2 int
					considerationsArray[considerationCount], calculations2 = Scene{
						board: s.board,
						c: c,
						color: s.color,
						fields: s.fields,
					}.consider(depth, startDepth)
					calculations += calculations2
				}
				considerationCount++
			}
		}
	}
	
	var tempScene Scene
	var boardCpy Board
	bestIdea = Consideration{score: math.Inf(-1)}
	moves = 0
	
	for i := 0; i < considerationCount; i++ {
		var ideas []Consideration
		if concurrency {
			chanIn := <-considerations
			ideas = chanIn.c
			calculations += chanIn.calculations
		} else {
			ideas = considerationsArray[i]
		}
		for _, idea := range ideas {
			if idea.score > bestIdea.score {
				tempScene = s
				boardCpy = *s.board
				tempScene.board = &boardCpy
				tempScene.c = idea.start
				tempScene.movePiece(idea.end)
				
				
			//	if tempScene.getField(scene_whiteCheck + sceneField(tempScene.color)) { // If this color's king is in check...
				if tempScene.checkCheck() {
					continue // ...then don't do `moves++` and don't consider this move: you cannot make a move that puts your own king in check.
				}
				
				bestIdea = idea
			}
			moves++
		}
	}
	
	return
}
// Goes over the moves this piece can make, BUT ALSO king-in-check moves (and king-through check castling), and returns an unsorted list.
// The reciever isn't a pointer, because the scene .x & .y fields might get changed while this is processing in a goroutine.
func (s Scene)consider(depth, startDepth int) (ideas []Consideration, calculations int) {
	ideas = make([]Consideration, 16)[:0]
	
	var tempScene Scene
	var tempBoard Board
	
	
	s.moves(func(c coord, specialMove *specialMove_t) bool {
		// Okay, this move is legal (with few exceptions), but what are the counter-moves?
		// Run goodMoves on the opponent color!
		// (Unless depth == 0!)
		var score float64 = s.board[c.y][c.x].val()
		
		if depth != 0 {
			// Copy the board and scene.
			tempBoard = *s.board
			tempScene = s
			tempScene.board = &tempBoard
			
			// If this was a special move, apply the special part, and check for legality if needed:
			if specialMove != nil {
				/*if specialMove.specialMoveType == specialMove_castle {
					// (TODO NEXT NEXT) Make sure that the king is not in check, will not pass though check, and will not move into check:  ~~ ACTUALLY shouldn't you put this at the end of goodMoves()?
					isCastle = true
				}*/
				
				if specialMove.deletePiece.x != 8 {
					tempScene.board[specialMove.deletePiece.y][specialMove.deletePiece.x].pType = piece_nil
				}
				if specialMove.setPiece_coord.x != 8 {
					tempScene.board[specialMove.setPiece_coord.y][specialMove.setPiece_coord.x] = specialMove.setPiece_piece
				}
			}
			
			// Make the move in the temporary scene.
			tempScene.movePiece(c)
			
		//	tempBoard[y][x] = tempBoard[s.y][s.x]
		//	tempBoard[s.y][s.x] = Piece{}
			
			// Find potential opponent moves:
			tempScene.color = 1-tempScene.color
			counterMove, moves, calculations2 := tempScene.goodMoves(depth - 1, startDepth)
			calculations += calculations2
			
			if moves != 0 {
				score -= counterMove.score * (float64(depth) / float64(startDepth))
			}
			
		}
		// If this is the lowest layer of depth, then you'd think that there's no need to add a Consideration if its score is zero.
		// However this causes this program to make bad moves.
		if specialMove != nil {
			ideas = append(ideas, Consideration{score, s.c, c, specialMove.specialMoveType})
		} else {
			ideas = append(ideas, Consideration{score, s.c, c, 0})
		}
		
		// A move has been done by this consider() instance.
		// Note that the above if block can also add to calculations int.
		calculations++
		
		return false
	})
	
	return
}
