package main

/*
	TODO LIST:
	.	Avoid draws.
	.	Add to rules: cannot make a move that puts the king in check. Perhaps add the king coords into Scene and make a s.checkCheck() function.
	.	Make computation work in a way that it can be stopped whenever, can calculate while the opponent is thinking, and not discard the path predicted while the opponent was thinking when it was chosen, and not discard the path chosen by Dumber.
	.	HEY! what about temp. scenes? e.g. &Scene{STUFF}.isLegal()
	See the file "./OTHER/IDEAS"!
*/

import (
	"fmt"
	"os/exec"
)

type Board [8][8]Piece

const (
	debugMode = true
	// Extra-sanity checks.
	insanityChecks = false
)

var (
	setupBoard Board = Board{
		{Piece{black, rook}, Piece{black, knight}, Piece{black, bishop}, Piece{black, queen}, Piece{black, king}, Piece{black, bishop}, Piece{black, knight}, Piece{black, rook}},
		{Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}},
		{},
		{},
		{},
		{},
		{Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}},
		{Piece{white, rook}, Piece{white, knight}, Piece{white, bishop}, Piece{white, queen}, Piece{white, king}, Piece{white, bishop}, Piece{white, knight}, Piece{white, rook}},
	}
	
	scene = Scene{
		color: white,
	}
	
	considerations = make(chan Consideration)
	
	//TEST = make(chan int)
)

func init() {
	sceneBoard := setupBoard
	scene.board = &sceneBoard
}

func main() {
	for { // make a move and recieve an opponent move
		/*go func(){
			var min, max int
			min = 99999999999999999
			var averaged, i float64
			for length := range TEST {
				if length > max {
					max = length
				}
				if length < min {
					min = length
				}
				averaged = averaged+float64(length)
				i++
			}
			averaged /= i
			fmt.Printf("min: %v; max: %v; avg: %v\n", min, max, averaged)
		}()*/
		bestOption, moves, calculations := scene.goodMoves(4, 4)
		
		if moves == 0 {
			fmt.Println("I RESIGN 😿")
			return
		}
		fmt.Printf("(I THINK) %v POSSIBLE MOVES, %v INTERNALLY CONSIDERED MOVES.\n", moves, calculations)
		
		/*if bestOption.score == -1 {
			fmt.Println("I RESIGN 😿😿😿😿\nCANNOT THINK OF WHERE TO MOVE")
			return
		}*/
		
		// move piece and tell:
	//	scene.board[bestOption.endY][bestOption.endX] = scene.board[bestOption.startY][bestOption.startX]
	//	scene.board[bestOption.startY][bestOption.startX] = Piece{}
		scene.c = bestOption.start
		scene.movePiece(bestOption.end)
		
		fmt.Printf("I MOVED %s TO %s\n", bestOption.start.String(), bestOption.end.String())
		
		exec.Command("espeak", "Ready.").Start()
		
		var luserStart, luserEnd coord
		for {
			var input string
			var ok bool = false
			
			fmt.Print("WHAT'S YOUR MOVE?\n")
			for {
				fmt.Print("FROM: ")
				fmt.Scan(&input)
				luserStart, ok = decodeCoord(input)
				if ok {
					break
				} else {
					fmt.Println("THAT IS AN INVALID COORDINATE")
				}
			}
			
			for {
				fmt.Print("TO: ")
				fmt.Scan(&input)
				luserEnd, ok = decodeCoord(input)
				if ok {
					break
				} else {
					fmt.Println("THAT IS AN INVALID COORDINATE")
				}
			}
			
			// Check for input errors:
			testScene := scene
			testScene.c = luserStart
			testScene.color = 1 - scene.color
			if !testScene.isLegal(luserEnd) {
				fmt.Println("THAT IS NOT A LEGAL MOVE!")
				continue
			}
			break
		}
		
		scene.c = luserStart
		scene.movePiece(luserEnd)
	//	scene.board[luserEndY][luserEndX] = scene.board[luserY][luserX]
	//	scene.board[luserY][luserX] = Piece{}
		
		fmt.Println("THANKS LUSER\n\n")
	}

}
