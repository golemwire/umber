package main

import (
	"fmt"
)

type Scene struct {
	board *Board
	// Location of the square/piece in question
	c coord
	// Which color should move
	color Color
	// See the comment before the sceneField enumeration
	fields byte // TODO: make the rules use these
}

type sceneField int
// Fields for castling. Listed from the LSB to the MSB:
// Code that relies on these fields being exactly this way are labeled with "LABEL:S_FIELDS"
const (
	scene_whiteCheck sceneField = iota // whether the king is in check
	scene_blackCheck
	
	scene_rook0_0 // Whether it has moved OR been captured.
	scene_rook7_0
	scene_blackKingMoved
	
	scene_rook0_7
	scene_rook7_7
	scene_whiteKingMoved
)

func (s *Scene)getField(field sceneField) bool {
	return (s.fields >> field) & 0b1 == 1
}
func (s *Scene)setField(field sceneField) {
	s.fields |= byte(1) << field
}
func (s *Scene)clearField(field sceneField) {
	s.fields &= ^(byte(1) << field) // TODO: this does work, right?
}

func (s *Scene)movePiece(end coord) {
	if insanityChecks {
		if !end.withinBounds() {
			panic(fmt.Sprintf("Out of bounds: (%v,%v)", end.x, end.y))
		}
		if s.board[s.c.y][s.c.x].pType == piece_nil {
			panic(fmt.Sprintf("Piece being moved doesn't exist: (%v,%v)", s.c.x, s.c.y))
		}
		if s.c.x == end.x && s.c.y == end.y {
			panic(fmt.Sprintf("Piece being moved to the same spot: (%v,%v)", s.c.x, s.c.y))
		}
		if s.board[s.c.y][s.c.x].color == s.board[end.y][end.x].color && s.board[end.y][end.x].pType != piece_nil {
			panic(fmt.Sprintf("Capturing a piece of the same color: (%v,%v) capturing (%v,%v)", s.c.x, s.c.y, end.x, end.y))
		}
	}
	// Record whether a rook moved. For castling rules:
	if s.board[s.c.y][s.c.x].pType == rook {
		switch {
		case s.c.x == 0 && s.c.y == 0:
			s.setField(scene_rook0_0)
		case s.c.x == 7 && s.c.y == 0:
			s.setField(scene_rook7_0)
		case s.c.x == 0 && s.c.y == 7:
			s.setField(scene_rook0_7)
		case s.c.x == 7 && s.c.y == 7:
			s.setField(scene_rook7_7)
		}
	}
	// Record whether a rook was captured.
	// This way, a rook can't be captured, replaced with the other rook, and then castled with.
	// For castling rules:
	if s.board[end.y][end.x].pType == rook {
		switch {
		case s.c.x == 0 && s.c.y == 0:
			s.setField(scene_rook0_0)
		case s.c.x == 7 && s.c.y == 0:
			s.setField(scene_rook7_0)
		case s.c.x == 0 && s.c.y == 7:
			s.setField(scene_rook0_7)
		case s.c.x == 7 && s.c.y == 7:
			s.setField(scene_rook7_7)
		}
	}
	// Record whether a king moved. For castling rules:
	if s.board[s.c.y][s.c.x].pType == king {
		switch s.board[s.c.y][s.c.x].color {
		case black:
			s.setField(scene_blackKingMoved)
		case white:
			s.setField(scene_whiteKingMoved)
		}
	}
	
	/*// TODO NEXT: change the king-in-check fields if needed!
	// TODO: INEFFICIENT!!:
	// Find the kings:
	var blackKing, whiteKing coord
	var i coord
	for i.y = 0; i.y < 8; i.y++ {
		for i.x = 0; i.x < 8; i.x++ {
			if s.board[i.y][i.x].pType == piece_nil { continue }
			
			if s.board[i.y][i.x].pType == king {
				if s.board[i.y][i.x].color == black {
					blackKing = i
				} else {
					whiteKing = i
				}
			}
		}
	}
	
	for i.y = 0; i.y < 8; i.y++ {
		for i.x = 0; i.x < 8; i.x++ {
			if s.board[i.y][i.x].pType == piece_nil { continue }
			
			// TODO NEXT: Using isLegal like this is totally bad! Make a function like `(s *Scene)legalMoves(legalMove func(c coord)(stopEnumerating bool))`, and make things use it.
			// TODO: Make isLegal and legalMoves use the scene.fields.
			
			// TODO (after the above!): check the king in check fields before you do each:
			if s.isLegal(blackKing) {
				s.setField(scene_blackCheck)
			}
			if s.isLegal(whiteKing) {
				s.setField(scene_whiteCheck)
			}
		}
	}*/
	
	s.board[end.y][end.x] = s.board[s.c.y][s.c.x]
	s.board[s.c.y][s.c.x] = Piece{}
}
