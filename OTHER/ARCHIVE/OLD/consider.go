package main


type Consideration struct {
	score float64
	startX, startY int
	endX, endY int
}

// Goes over the moves this piece can make, and returns an unsorted list.
// TODO: It also considers opponent counter-moves.
func (s Scene)consider(depth, startDepth int) (ideas []Consideration) {
	ideas = make([]Consideration, 4)[:0]
	
	var tempBoard Board
	
	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			if s.isLegal(x, y) {
				// Okay, this move is legal, but what are the counter-moves?
				// Run goodMoves on the opponent color!
				// (Unless depth = 0!)
				var score float64 = s.board[y][x].val()
				
				if depth != 0 {
					tempBoard = *s.board
					
					tempBoard[y][x] = tempBoard[s.y][s.x]
					tempBoard[s.y][s.x] = Piece{}
					
					counterIdeas := goodMoves(&tempBoard, 1-s.color, depth - 1, startDepth)
					
					if len(counterIdeas) != 0 {
						score -= counterIdeas[0].score * (float64(depth) / float64(startDepth))
					}
				}
				
				ideas = append(ideas, Consideration{score, s.x, s.y, x, y})
			}
		}
	}
	
	/*var bestIdea = Consideration{score: -1}
	for _, c := range ideas {
		if c.score > bestIdea.score {
			bestIdea = c
		}
	}
	
	considerations <- bestIdea*/
	return
}