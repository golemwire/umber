package main

import (
	"fmt"
)

type Board [8][8]Piece

var (
	setupBoard Board = Board{
		{Piece{black, rook}, Piece{black, knight}, Piece{black, bishop}, Piece{black, queen}, Piece{black, king}, Piece{black, bishop}, Piece{black, knight}, Piece{black, rook}},
		{Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}},
		{},
		{},
		{},
		{},
		{Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}},
		{Piece{white, rook}, Piece{white, knight}, Piece{white, bishop}, Piece{white, queen}, Piece{white, king}, Piece{white, bishop}, Piece{white, knight}, Piece{white, rook}},
	}
	
	mainBoard = setupBoard
	myColor = white
	
	considerations = make(chan Consideration)
)

func main() {
	for { // make a move and recieve an opponent move
		ideas := goodMoves(&mainBoard, myColor, 4, 4)
		
		if len(ideas) == 0 {
			fmt.Println("I RESIGN 😿")
			return
		}
		fmt.Printf("%v MOVES CONSIDERED. MANY MORE ANALYZED.\n", len(ideas))
		
		bestOption := ideas[0]
		/*if bestOption.score == -1 {
			fmt.Println("I RESIGN 😿😿😿😿\nCANNOT THINK OF WHERE TO MOVE")
			return
		}*/
		
		// move piece and tell:
		mainBoard[bestOption.endY][bestOption.endX] = mainBoard[bestOption.startY][bestOption.startX]
		mainBoard[bestOption.startY][bestOption.startX] = Piece{}
		fmt.Printf("I MOVED %v %v TO %v %v\n", bestOption.startX, bestOption.startY, bestOption.endX, bestOption.endY)
		
		var luserX, luserY int
		var luserEndX, luserEndY int
		for {
			fmt.Print("WHAT'S YOUR MOVE?\n")
			fmt.Print("FROM X: ")
			fmt.Scan(&luserX)
			fmt.Print("FROM Y: ")
			fmt.Scan(&luserY)
			fmt.Print("TO X: ")
			fmt.Scan(&luserEndX)
			fmt.Print("TO Y: ")
			fmt.Scan(&luserEndY)
			
			// Check for input errors:
			
			if !withinBounds(luserX, luserY) || !withinBounds(luserEndX, luserEndY) {
				fmt.Println("THAT SQUARE (OR SQUARES) IS OFF THE BOARD...")
				continue
			}
			
			if !(&Scene{
				board: &mainBoard,
				x: luserX,
				y: luserY,
				color: 1 - myColor,
			}).isLegal(luserEndX, luserEndY) {
				fmt.Println("THAT'S NOT A LEGAL MOVE!")
				continue
			}
			break
		}
		
		mainBoard[luserEndY][luserEndX] = mainBoard[luserY][luserX]
		mainBoard[luserY][luserX] = Piece{}
		
		fmt.Println("THANKS LUSER\n\n")

		
	}

}

func withinBounds(x, y int) bool {
	return x>=0 && x<8 &&
	       y>=0 && y<8
}
