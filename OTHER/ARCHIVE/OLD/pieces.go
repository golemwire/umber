package main

type Scene struct {
	board *Board
	// Location of the piece in question
	x, y int
	// Which color should move
	color Color
}

type Piece struct {
	color Color
	pType PType
}

type Color uint8
const (
	white Color = iota
	black
)

type PType int
const (
	piece_nil PType = iota
	pawn
	knight
	bishop
	rook
	queen
	king
)

func (p *Piece)val() float64 {
	// The King should not be math.Inf(1), because, then, predictions involving ∞ will always have the same score, regardless of the probability or how far ahead the move is;
	// at least, I think that's why my king moved itself into check...
	return [...]float64{
		0,      // piece_nil
		1,      // pawn
		3.2,    // knight
		3.33,   // bishop
		5.1,    // rook
		8.8,    // queen
		65536,  // king (an arbitrarily high number) LATER: is this high enough? Will it be added to itself too many times in goodMoves()?
	}[int(p.pType)]
}

// Just returns whether a move from c.x,c.y to moveX,moveY is legal.
func (s *Scene)isLegal(moveX, moveY int) bool {
	piece := s.board[s.y][s.x]
	
	// For the "sliding"? pieces: the rook, bishop, and queen.
	var dist, x, y, xVel, yVel int
	x, y = s.x, s.y
	slide := func() bool {
		// Stop one square short, because it may be occupied (like if capturing)
		dist--
		for i := 0; i < dist; i++ {
			// Also, skip the first square (because it is occupied by the moving piece)
			x += xVel ; y += yVel
			
			if s.board[y][x].pType != piece_nil {
				// Cannot move through a piece
				return false
			}
		}
		return true
	}
	// For the rook & queen.
	// (Returns whether the move is definitely illegal.)
	straightSlideInit := func() bool {
		if s.x == moveX {
			if moveY < s.y {
				yVel = -1
				dist = s.y - moveY
			} else {
				yVel = 1
				dist = moveY - s.y
			}
			return true
		} else if s.y == moveY {
			if moveX < s.x {
				xVel = -1
				dist = s.x - moveX
			} else {
				xVel = 1
				dist = moveX - s.x
			}
			return true
		}
		return false
	}
	// For the bishop & queen.
	diagonalSlideInit := func() bool {
		if moveX - s.x == moveY - s.y { // backslash-diagonal
			if moveX < s.x {
				xVel = -1
				yVel = -1
				dist = s.x - moveX
			} else {
				xVel = 1
				yVel = 1
				dist = moveX - s.x
			}
			return true
		} else if moveX - s.x == s.y - moveY { // forwardslash-diagonal
			if moveX < s.x {
				xVel = -1
				yVel = 1
				dist = s.x - moveX
			} else {
				xVel = 1
				yVel = -1
				dist = moveX - s.x
			}
			return true
		}
		return false
	}
	
	switch {
	// Can't move a non-existant piece:
	case piece.pType == piece_nil:
		return false
	/*// Can't move a piece to itself: REDUNDANT
	case s.x==moveX && s.y==moveY:
		return false*/
	// Can't move opponent's piece:
	case s.color != piece.color:
		return false
	// Can't capture your own piece:
	case s.board[moveY][moveX].pType!=piece_nil && s.board[moveY][moveX].color == s.color:
		return false
	}
	
	switch piece.pType {
	case pawn:
		// Moving one square ahead (white) or backwards (black):
		if (s.color == white && s.y-1 == moveY) ||
		   (s.color == black && s.y+1 == moveY) {
			switch {
			// Moving forward one (or two) moves w/o capture:
			case s.x == moveX:
				return s.board[moveY][moveX].pType == piece_nil
			// Capturing diagonally:
			case s.x-1==moveX || s.x+1==moveX:
				return s.board[moveY][moveX].pType != piece_nil &&
				       s.board[moveY][moveX].color != s.color
			default:
				return false
			}
		} else if (s.color == white && s.y == 6 && moveY == 4) ||
		          (s.color == black && s.y == 1 && moveY == 3) { // Moving two squares ahead:
			// Not moving to the left or the right, and not capturing:
			return s.x == moveX && s.board[moveY][moveX].pType == piece_nil
		}
		/*cx, cy = x, y-1
		if withinBounds(cx, cy) && c.board[cy][cx].pType==piece_nil {
			ideas = append(ideas, Consideration{0, x, y, cx, cy})
		}
		
		
		cx, cy = x-1, y-1
		if withinBounds(cx, cy) && c.board[cy][cx].pType!=piece_nil && board[cy][cx].color!=myColor {
			ideas = append(ideas, Consideration{c.board[cy][cx].val(), x, y, cx, cy})
		}
		cx, cy = x+1, y-1
		if withinBounds(cx, cy) && c.board[cy][cx].pType!=piece_nil && board[cy][cx].color!=myColor {
			ideas = append(ideas, Consideration{c.board[cy][cx].val(), x, y, cx, cy})
		}*/
	case knight:
		if (moveX == s.x-2 && moveY == s.y-1) ||
		   (moveX == s.x-2 && moveY == s.y+1) ||
		   (moveX == s.x+2 && moveY == s.y-1) ||
		   (moveX == s.x+2 && moveY == s.y+1) ||
		   (moveX == s.x-1 && moveY == s.y-2) ||
		   (moveX == s.x+1 && moveY == s.y+2) ||
		   (moveX == s.x-1 && moveY == s.y+2) ||
		   (moveX == s.x+1 && moveY == s.y-2) {
			return true
		}
	case bishop:
		if diagonalSlideInit() == false {return false}
		
		return slide()
	case rook:
		if straightSlideInit() == false {return false}
		
		return slide()
	case queen:
		/*ssi := straightSlideInit()
		var sSlide bool
		if ssi {sSlide = slide()}
		dsi := diagonalSlideInit()
		var dSlide bool
		if dsi {dSlide = slide()}
		return (ssi && sSlide) || (dsi && dSlide)*/
		ss := straightSlideInit() && slide()
		ds := diagonalSlideInit() && slide()
		
		return ss || ds
	case king:
		// Moving within 1 square (including diagonally)
		if moveX >= s.x-1 && moveX <= s.x+1 &&
		   moveY >= s.y-1 && moveY <= s.y+1 {
			return true
		}
	
	default: panic("WHAT THE HEK WHAT PIECE IS THIS 🙃")
	}
	
	//fmt.Fprintf(os.Stderr, "Error: exited main isLegal switch\n")
	return false
}