package main

import (
	"sort"
)

// Finds moves that the given color can play on the given board, sorted by how good the move is.
func goodMoves(board *Board, color Color, depth, startDepth int) (ideas []Consideration) {
	considerationCount := 0
	considerations := make(chan []Consideration)
	ideas = make([]Consideration, 16)[:0]
	
	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			if mainBoard[y][x].pType == piece_nil {continue}
			
			if mainBoard[y][x].color == color {
				go func(s Scene) {
					considerations <- s.consider(depth, startDepth)
				}(Scene{
					board: board,
					x: x,
					y: y,
					color: color,
				})
				considerationCount++
			}
		}
	}
	
	for i := 0; i < considerationCount; i++ {
		ideas = append(ideas, <-considerations...)
	}
	
	sort.Slice(ideas, func(i, j int) bool {
		if ideas[i].score == ideas[j].score {
			return (ideas[i].startX + ideas[i].startY*8) | ((ideas[i].endX + ideas[i].endY*8)*64) < 
			       (ideas[j].startX + ideas[j].startY*8) | ((ideas[j].endX + ideas[j].endY*8)*64)
		}
		return ideas[i].score > ideas[j].score
	})
	
	return
}