package main

import (
	"fmt"
	"strings"
)

// If x == 8, then the coord is considered "null"
type coord struct {
	x, y int8
}

func (a coord)plus(b coord) coord {
	return coord{a.x + b.x, a.y + b.y}
}

func (c coord)withinBounds() bool {
	return c.x>=0 && c.x<8 &&
	       c.y>=0 && c.y<8
}

func (c coord)String() string {
	if insanityChecks {
		if c.x == 8 { panic(fmt.Sprintf("Null coord: (%v,%v)", c.x, c.y)) }
	}
	
	ret := make([]byte, 2)
	
	ret[0] = "ABCDEFGH"[c.x]
	ret[1] = "87654321"[c.y]
	
	return string(ret)
}

// Returns the decoded coordinate, and whether the input was valid.
func decodeCoord(input string) (c coord, ok bool) {
	if len(input) != 2 {
		ok = false
		return
	}
	index := strings.IndexByte("ABCDEFGH", input[0])
	if index == -1 {
		index = strings.IndexByte("abcdefgh", input[0])
		if index == -1 {
			ok = false
			return
		}
	}
	c.x = int8(index)
	
	index = strings.IndexByte("87654321", input[1])
	if index == -1 {
		ok = false
		return
	}
	c.y = int8(index)
	
	ok = true
	return
}
