package main

type Piece struct {
	color Color
	pType PType
}

type Color uint8
const (
	white Color = 0
	black Color = 1
)

type PType uint8
const (
	piece_nil PType = iota
	pawn
	knight
	bishop
	rook
	queen
	king
)

func (p *Piece)val() float64 {
	// The King should not be math.Inf(1), because, then, predictions involving ∞ will always have the same score, regardless of the probability or how far ahead the move is;
	// at least, I think that's why my king moved itself into check...
	return [...]float64{
		0,      // piece_nil
		1,      // pawn
		3.2,    // knight
		3.33,   // bishop
		5.1,    // rook
		8.8,    // queen
		65536,  // king (an arbitrarily high number) LATER: is this high enough? Will it be added to itself too many times in goodMoves()? Actually, try making it something smaller, like 20. Check the Wikipedia article. ~~ Actually make it so high that it only competes with scores involving other kings. Just make sure it never overflows. ~~ Overflows?
	}[int(p.pType)]
}

func (p Piece)String() string {
	switch p.color {
	case white:
		return /*"[37;1m" + */[...]string{
			"  ", // piece_nil
			"♙ ",  // pawn
			"♘ ",  // knight
			"♗ ",  // bishop
			"♖ ",  // rook
			"♕ ",  // queen
			"♔ ",  // king
		}[int(p.pType)]
	case black:
		return /*"[30;1m" + */[...]string{
			"  ", // piece_nil
			"♟ ",  // pawn
			"♞ ",  // knight
			"♝ ",  // bishop
			"♜ ",  // rook
			"♛ ",  // queen
			"♚ ",  // king
		}[int(p.pType)]
	}
	panic("FATAL")
}
