package main

/*
	Design before you type.
	TODO LIST:
	.	Avoid draws. SEE https://www.chess.com/terms/draw-chess
	.	Add to rules: cannot make a move that puts the king in check. Perhaps add the king coords into Scene and make a s.checkCheck() function.
	.	Make computation work in a way that it can be stopped whenever, can calculate while the opponent is thinking, and not discard the path predicted while the opponent was thinking when it was chosen, and not discard the path chosen by Dumber.
	.	HEY! what about temp. scenes? e.g. &Scene{STUFF}.isLegal()
	See the file "./OTHER/IDEAS"!
*/

import (
	"fmt"
	"strings"
	"strconv"
	"time"
	"os"
	"os/exec"
)

const (
	debugMode = true
	// Extra-sanity checks.
	insanityChecks = true
)

var (
	setupBoard Board = Board{
		{Piece{black, rook}, Piece{black, knight}, Piece{black, bishop}, Piece{black, queen}, Piece{black, king}, Piece{black, bishop}, Piece{black, knight}, Piece{black, rook}},
		{Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}, Piece{black, pawn}},
		{},
		{},
		{},
		{},
		{Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}, Piece{white, pawn}},
		{Piece{white, rook}, Piece{white, knight}, Piece{white, bishop}, Piece{white, queen}, Piece{white, king}, Piece{white, bishop}, Piece{white, knight}, Piece{white, rook}},
	}
	/*setupBoard Board = Board{
		{Piece{black, king}},
		{},
		{Piece{white, king}},
		{},
		{},
		{},
		{},
		{},
	}*/
	scene = Scene{
		color: white,
	}
	
	// Whether Umber should play against itself
	bothAuto = false
	
	lastMoves [2]struct{s, e coord}
	repeatedMoves int
)

func init() {
	sceneBoard := setupBoard
	scene.board = &sceneBoard
}

func main() {
	var goodMovesDepth int
	switch len(os.Args) {
	case 1:
		goodMovesDepth = 4
	case 3:
		if os.Args[2] == "--both-auto" {
			bothAuto = true
		} else {
			fmt.Println("Error: invalid argument(s)")
			os.Exit(1)
		}
		fallthrough
	case 2:
		var err error
		goodMovesDepth, err = strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Println("Error: invalid argument")
			os.Exit(1)
		}
	default:
		fmt.Println("Error: incorrect amount of arguments")
		os.Exit(1)
	}
	
	if !bothAuto { fmt.Println("WELCOME") }
	fmt.Println(scene.board.String())
	if !bothAuto { fmt.Println("LET'S PLAY CHESS") }
	
	for { // MAIN LOOP
	
		if bothAuto {
			
		} else {
			// Cool feature: process in background, and guess what your best move should be.
			// LATER: Perhaps add a hint feature where you can ask for a hint
			guess := make(chan struct{bestOption Consideration; moves int})
			go func() {
				bestOption, moves, _ := scene.goodMoves(4, 4)
				guess <- struct{bestOption Consideration; moves int}{bestOption, moves}
			}()
			
			var luserStart, luserEnd coord
			var luserSpecialMove *specialMove_t
			for {
				luserSpecialMove = nil
				var input string
				var ok bool = false
				
				fmt.Print("WHAT'S YOUR MOVE?\n")
				// Get move coordinates:
				for {
					fmt.Print("FROM: ")
					fmt.Scan(&input)
					luserStart, ok = decodeCoord(input)
					if ok {
						break
					} else {
						fmt.Println("THAT IS AN INVALID COORDINATE")
					}
				}
				
				for {
					fmt.Print("TO: ")
					fmt.Scan(&input)
					luserEnd, ok = decodeCoord(input)
					if ok {
						break
					} else {
						fmt.Println("THAT IS AN INVALID COORDINATE")
					}
				}
				
				// Check for special moves:
				switch { // TODO NEXT
				case (scene.board[luserStart.y][luserStart.x].pType == pawn) && (scene.color==white && luserEnd.y==0) || (scene.color==black && luserEnd.y==7):
					luserSpecialMove = &specialMove_t{
						specialMoveType: specialMove_promotion,
						deletePiece: coord{x: 8},
						setPiece_piece: Piece{color: scene.color},
						setPiece_coord: luserEnd,
					}
					
					// Get the piece to be promoted to:
					fmt.Println("PAWN PROMOTION:\n" +
					"WHICH PIECE DO YOU WANT?\n" +
					"QUEEN, ROOK, BISHOP, KNIGHT? [Q, R, B, K] ")
					var input string
					inputLoop: for {
						_, err := fmt.Scan(&input)
						if err == nil {
							switch strings.ToLower(input) {
							case "q": fallthrough
							case "queen":
								luserSpecialMove.setPiece_piece.pType = queen
								break inputLoop
							case "r": fallthrough
							case "rook":
								luserSpecialMove.setPiece_piece.pType = rook
								break inputLoop
							case "b": fallthrough
							case "bishop":
								luserSpecialMove.setPiece_piece.pType = bishop
								break inputLoop
							case "k": fallthrough
							case "knight":
								luserSpecialMove.setPiece_piece.pType = knight
								break inputLoop
							}
						}
						fmt.Println("INVALID\n")
					}
				}
				
				// Check for input errors / move legality:
				testScene := scene
				testScene.c = luserStart
				if !testScene.isLegal(luserEnd) { // If move is illegal:
					fmt.Println("THAT IS NOT A LEGAL MOVE!")
					continue
				} else { // If move is legal, checkCheck:
					boardCpy := *scene.board
					testScene.board = &boardCpy
					testScene.movePiece(luserEnd)
					testScene.applySpecial(luserSpecialMove)
					
				//	if testScene.getField(scene_whiteCheck + sceneField(testScene.color)) {
					if testScene.checkCheck() { // If this color's king is in check:
						// You cannot make a move that puts/keeps your own king in check
						fmt.Println("THAT IS NOT A LEGAL MOVE!\nYOUR KING WOULD BE IN CHECK")
						continue
					}
				}
				break
			}
			
			fmt.Println("THANKS USER")
			fmt.Print("MY GUESS: ")
			theGuess := <-guess // Do movePiece() after, just in case processing is still being done
			
			scene.c = luserStart
			scene.movePiece(luserEnd)
			scene.applySpecial(luserSpecialMove)
		//	scene.board[luserEndY][luserEndX] = scene.board[luserY][luserX]
		//	scene.board[luserY][luserX] = Piece{}
			
			fmt.Print(theGuess.bestOption.start.String(), " TO ", theGuess.bestOption.end.String(), "\n\n\n")
			
			// Switch to other color:
			scene.color = 1-scene.color
		}

		bestOption, moves, calculations := scene.goodMoves(goodMovesDepth, goodMovesDepth)
		
		// Did the chess engine lose?
		if moves == 0 {
			fmt.Println(scene.board.String())
			if scene.checkCheck() {
				fmt.Println("CHECKMATE! YOU ARE SMARTER THAN UMBER 😿") // LABEL: INCLUDESNAME
				exec.Command("espeak", "CHECKMATE!").Start()
			} else {
				fmt.Println("STALEMATE! 😿")
				exec.Command("espeak", "STALEMATE!").Start()
			}
			return
		}
		
		fmt.Printf("%v POSSIBLE MOVES, %v INTERNALLY CONSIDERED MOVES.\n", moves, calculations)
		
		// move piece and tell:
	//	scene.board[bestOption.endY][bestOption.endX] = scene.board[bestOption.startY][bestOption.startX]
	//	scene.board[bestOption.startY][bestOption.startX] = Piece{}
		scene.c = bestOption.start
		scene.movePiece(bestOption.end)
		scene.applySpecial(&bestOption.specialMove)
		
		fmt.Printf("I MOVED %s TO %s\n", bestOption.start.String(), bestOption.end.String())
		
		/*// Get check state:
		var check bool
		whiteCheck, blackCheck := scene.checkCheck()
		switch scene.color {
		case white: check = whiteCheck
		case black: check = blackCheck
		}
		
		scene.color = 1-scene.color
		if moves == 0 {
			if check {
			fmt.Println("CHECKMATED! YOU ARE LESS SMART THAN DUMBER THE CHESS ENGINE LOL 😿") // LABEL: INCLUDESNAME TODO: change
			exec.Command("espeak", "CHECKMATED!").Start()
			fmt.Println(scene.board.String())
			return
		} else {
			exec.Command("espeak", "Ready.").Start()
		}*/
		
		// Switch back to original color:
		scene.color = 1-scene.color
		
		fmt.Println(scene.board.String()) // Display board
		
		if bothAuto {
			time.Sleep(time.Second / 8)
		} else {
			_, moves, _ = scene.goodMoves(2, 2)
			if moves == 0 { // Game over! The chess engine wins!
				fmt.Println(scene.board.String())
				if scene.checkCheck() {
					fmt.Println("CHECKMATED! YOU ARE LESS SMART THAN UMBER THE CHESS ENGINE 😸") // LABEL: INCLUDESNAME TODO: change
					exec.Command("espeak", "CHECKMATED!").Start()
				} else {
					fmt.Println("STALEMATE! IT CAME TO THIS 😿")
					exec.Command("espeak", "STALEMATE!").Start()
				}
				return
			} else {
			//	exec.Command("espeak", "Ready.").Start()
			}
		}
		
	}
	
	/*for {
		fmt.Println(scene.board.String())

		bestOption, moves, calculations := scene.goodMoves(4, 4)
		
		if moves == 0 {
			fmt.Println("I RESIGN 😿")
			exec.Command("espeak", "I RESIGN! I resign.").Start()
			return
		}
		fmt.Printf("(I THINK) %v POSSIBLE MOVES, %v INTERNALLY CONSIDERED MOVES.\n", moves, calculations)
		
		// move piece and tell:
	//	scene.board[bestOption.endY][bestOption.endX] = scene.board[bestOption.startY][bestOption.startX]
	//	scene.board[bestOption.startY][bestOption.startX] = Piece{}
		scene.c = bestOption.start
		scene.movePiece(bestOption.end)
		
		fmt.Printf("I MOVED %s TO %s\n", bestOption.start.String(), bestOption.end.String())
		
	//	exec.Command("espeak", "Ready.").Start()
		
		scene.color = 1-scene.color
	}*/
}
